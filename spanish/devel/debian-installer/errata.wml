#use wml::debian::template title="Erratas del instalador de Debian"
#use wml::debian::recent_list
#include "$(ENGLISHDIR)/devel/debian-installer/images.data"
#use wml::debian::translation-check translation="5513b0df9f9525c15c9a757a14ac534e8d3ac03e" maintainer="Laura Arjona Reina"

<h1>Erratas en «<humanversion />»</h1>

<p>
Esta es una lista de problemas conocidos en la versión «<humanversion />»
del instalador de Debian. Si usted no ve aquí listado su problema,
por favor envíenos un <a href="$(HOME)/releases/stable/amd64/ch05s04#submit-bug">informe de instalación</a>
describiéndolo.
</p>

<dl class="gloss">
<dt>GNOME puede fallar al iniciarse en algunas instalaciones de máquina virtual</dt>
  <dd>Durante las pruebas de la imagen de Stretch Alfa 4, se ha visto que GNOME
  puede fallar al iniciarse dependiendo de la instalación usada en máquinas virtuales.
  Parece que usar cirrus como una emulación del chip de vídeo funciona bien.
  <br/>
  <b>Estado:</b> Se está investigando.</dd>

<dt>Las instalaciones con escritorio pueden fallar si se utiliza solo el CD 1</dt>
<dd>Debido a restricciones de espacio en el primer CD, no caben en el CD 1 todos los 
paquetes que se esperan para el escritorio GNOME. Para una instalación exitosa, 
use fuentes de paquete adicionales (p. ej. un segundo CD o una réplica en red)
o use un DVD.
<br /> <b>Estado:</b> No es probable que se puedan hacer más esfuerzos para encajar paquetes
	en el CD 1.</dd>

<dt>Tema usado en el instalador</dt>
     <dd>Todavía no hay arte gráfico para Bullseye, y el instalador aún usa
     el tema de Buster.
     <br />

     <dt>LUKS2 es incompatible con el soporte a cifrado de disco de GRUB</dt>
     <dd>Se descubrió tarde que GRUB no tiene soporte para LUKS2.
     Esto significa que los usuarios que quieren usar
       <tt>GRUB_ENABLE_CRYPTODISK</tt> y evitar una partición
       <tt>/boot</tt> separada y sin cifrar no podrán hacerlo
       (<a href="https://bugs.debian.org/927165">#927165</a>).  En cualquier caso, 
       esta configuración no está soportada en el instalador, pero podría tener
       sentido al menos documentar esta limitación de manera más prominente
       y tener al menos la posibilidad de optar por LUKS1 durante el proceso de
       instalación.
     <br />
     <b>Estado:</b> Se han expresado algunas ideas en el informe de fallo; los mantenedores de cryptsetup
     han escrito <a href="https://cryptsetup-team.pages.debian.net/cryptsetup/encrypted-boot.html">documentación específica</a>.</dd>


<!-- las cosas deberían ir mejor a partir de Jessie Beta 2...
	<dt>Soporte GNU/kFreeBSD</dt>
	<dd>Las imágenes de instalación de GNU/kFreeBSD tienen varios 
	fallos importantes
	(<a href="https://bugs.debian.org/757985">#757985</a>,
	<a href="https://bugs.debian.org/757986">#757986</a>,
	<a href="https://bugs.debian.org/757987">#757987</a>,
	<a href="https://bugs.debian.org/757988">#757988</a>). Los adaptadores
	agradecerían manos que ayuden a volver a tener en forma el instalador!</dd>
-->

<!-- algo obsoleto por el primer "cambio importante" mencionado en el anuncio de 20140813...
	<dt>Accesibilidad del sistema instalado</dt>
	<dd>Aunque se usen tecnologías de accesibilidad durante el proceso de 
	instalación, puede que éstas no se habiliten de manera automática en el sistema
	instalado.
	</dd>
-->

<!-- se deja esto para un posible futuro uso...
	<dt>Las instalaciones de escritorio en i386 no funcionan usando sólo el CD#1</dt>
	<dd>Debido a las restricciones de espacio en el primer CD, no todos los paquetes
	esperados para el escritorio GNOME caben en el CD#1. Para una instalación exitosa,
	use fuentes de paquetes extra (p.ej. un segundo CD o una réplica en la red) o use
	un DVD.
	<br />
	<b>Estado:</b> No es probable que se puedan hacer más esfuerzos para encajar paquetes
	en el CD#1.
	</dd>
-->

<!-- ditto...
	<dt>Posibles problemas con el arranque UEFI en amd64</dt>
	<dd>Ha habido algunos informes de problemas para arrancar el instalador de
	Debian en modo UEFI en sistemas amd64. Algunos sistemas aparentemente no arrancan
	de manera fiable usando <code>grub-efi</code>, y algunos otros muestran problemas
	de corrupción de los gráficos al visualizar la pantalla inicial («splash») de la instalación.
	<br />
	Si encuentra alguno de estos problemas, por favor remita un informe de error dando el máximo
	detalle posible, tanto sobre los síntomas, como sobre su hardware - esto debería ayudar al 
	equipo a resolver estos fallos. Para rodear el problema, por ahora, pruebe a desactivar UEFI
	e instalar usando la <q>BIOS antigua («Legacy»)</q> o el <q>modo alternativo («Fallback»)</q>.
	<br />
	<b>Estado:</b> Pueden aparecer más correcciones de fallos en las distintas versiones de Wheezy.
	</dd>
-->

<!-- ditto...
	<dt>i386: se necesita más de 32mb de memoria para instalar</dt>
	<dd>
	La cantidad mínima de memoria que se necesita para instalar en i386
	es 48m, en lugar de los 32mb anteriores. Esperamos reducir los requisitos
	de nuevo a 32mb más adelante. Los requisitos de memoria pueden haber cambiado
	también para otras arquitecturas.
	</dd>
-->

</dl>
