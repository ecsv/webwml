<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several security vulnerabilities were discovered in tiff3, a library
providing support for the Tag Image File Format (TIFF). An attacker
could take advantage of these flaws to cause a denial-of-service
against an application using the libtiff4 or libtiffxx0c2 library
(application crash), or potentially execute arbitrary code with the
privileges of the user running the application.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
3.9.6-11+deb7u1.</p>

<p>We recommend that you upgrade your tiff3 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-610.data"
# $Id: $
