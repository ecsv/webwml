<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in OpenJDK, an
implementation of the Oracle Java platform, resulting in privilege
escalation, denial of service, newline injection in SMTP or use of
insecure cryptography.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
7u131-2.6.9-2~deb7u1.</p>

<p>We recommend that you upgrade your openjdk-7 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-954.data"
# $Id: $
