msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2006-08-12 17:14+0200\n"
"Last-Translator: unknown\n"
"Language-Team: unknown\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/intro/organization.data:15
msgid "delegation mail"
msgstr ""

#: ../../english/intro/organization.data:16
msgid "appointment mail"
msgstr ""

#. One male delegate
#. Pronoun tags with delegate combinations
#: ../../english/intro/organization.data:18
#: ../../english/intro/organization.data:22
msgid "<void id=\"male\"/>delegate"
msgstr ""

#. One female delegate
#: ../../english/intro/organization.data:20
#: ../../english/intro/organization.data:23
msgid "<void id=\"female\"/>delegate"
msgstr ""

#: ../../english/intro/organization.data:22
#: ../../english/intro/organization.data:25
msgid "<void id=\"he_him\"/>he/him"
msgstr ""

#: ../../english/intro/organization.data:23
#: ../../english/intro/organization.data:26
msgid "<void id=\"she_her\"/>she/her"
msgstr ""

#: ../../english/intro/organization.data:24
msgid "<void id=\"gender_neutral\"/>delegate"
msgstr ""

#: ../../english/intro/organization.data:24
#: ../../english/intro/organization.data:27
msgid "<void id=\"they_them\"/>they/them"
msgstr ""

#: ../../english/intro/organization.data:30
#: ../../english/intro/organization.data:32
msgid "current"
msgstr ""

#: ../../english/intro/organization.data:34
#: ../../english/intro/organization.data:36
msgid "member"
msgstr ""

#: ../../english/intro/organization.data:39
msgid "manager"
msgstr ""

#: ../../english/intro/organization.data:41
msgid "SRM"
msgstr ""

#: ../../english/intro/organization.data:41
msgid "Stable Release Manager"
msgstr ""

#: ../../english/intro/organization.data:43
msgid "wizard"
msgstr ""

#. we only use the chair tag once, for techctte, I wonder why it's here.
#: ../../english/intro/organization.data:45
msgid "chair"
msgstr ""

#: ../../english/intro/organization.data:48
msgid "assistant"
msgstr ""

#: ../../english/intro/organization.data:50
msgid "secretary"
msgstr ""

#: ../../english/intro/organization.data:52
msgid "representative"
msgstr ""

#: ../../english/intro/organization.data:54
msgid "role"
msgstr ""

#: ../../english/intro/organization.data:62
msgid ""
"In the following list, <q>current</q> is used for positions that are\n"
"transitional (elected or appointed with a certain expiration date)."
msgstr ""

#: ../../english/intro/organization.data:70
#: ../../english/intro/organization.data:82
msgid "Officers"
msgstr ""

#: ../../english/intro/organization.data:71
#: ../../english/intro/organization.data:108
msgid "Distribution"
msgstr ""

#: ../../english/intro/organization.data:72
#: ../../english/intro/organization.data:239
msgid "Communication and Outreach"
msgstr ""

#: ../../english/intro/organization.data:74
#: ../../english/intro/organization.data:242
msgid "Data Protection team"
msgstr ""

#: ../../english/intro/organization.data:75
#: ../../english/intro/organization.data:246
msgid "Publicity team"
msgstr ""

#: ../../english/intro/organization.data:77
#: ../../english/intro/organization.data:319
msgid "Membership in other organizations"
msgstr ""

#: ../../english/intro/organization.data:78
#: ../../english/intro/organization.data:347
msgid "Support and Infrastructure"
msgstr ""

#: ../../english/intro/organization.data:85
msgid "Leader"
msgstr ""

#: ../../english/intro/organization.data:87
msgid "Technical Committee"
msgstr ""

#: ../../english/intro/organization.data:103
msgid "Secretary"
msgstr ""

#: ../../english/intro/organization.data:111
msgid "Development Projects"
msgstr ""

#: ../../english/intro/organization.data:112
msgid "FTP Archives"
msgstr ""

#: ../../english/intro/organization.data:114
msgid "FTP Masters"
msgstr ""

#: ../../english/intro/organization.data:120
msgid "FTP Assistants"
msgstr ""

#: ../../english/intro/organization.data:126
msgid "FTP Wizards"
msgstr ""

#: ../../english/intro/organization.data:130
msgid "Backports"
msgstr ""

#: ../../english/intro/organization.data:132
msgid "Backports Team"
msgstr ""

#: ../../english/intro/organization.data:136
msgid "Release Management"
msgstr ""

#: ../../english/intro/organization.data:138
msgid "Release Team"
msgstr ""

#: ../../english/intro/organization.data:147
msgid "Quality Assurance"
msgstr ""

#: ../../english/intro/organization.data:148
msgid "Installation System Team"
msgstr ""

#: ../../english/intro/organization.data:149
msgid "Debian Live Team"
msgstr ""

#: ../../english/intro/organization.data:150
msgid "Release Notes"
msgstr ""

#: ../../english/intro/organization.data:152
msgid "CD/DVD/USB Images"
msgstr ""

#: ../../english/intro/organization.data:154
msgid "Production"
msgstr ""

#: ../../english/intro/organization.data:161
msgid "Testing"
msgstr ""

#: ../../english/intro/organization.data:163
msgid "Cloud Team"
msgstr ""

#: ../../english/intro/organization.data:167
msgid "Autobuilding infrastructure"
msgstr ""

#: ../../english/intro/organization.data:169
msgid "Wanna-build team"
msgstr ""

#: ../../english/intro/organization.data:176
msgid "Buildd administration"
msgstr ""

#: ../../english/intro/organization.data:193
msgid "Documentation"
msgstr ""

#: ../../english/intro/organization.data:198
msgid "Work-Needing and Prospective Packages list"
msgstr ""

#: ../../english/intro/organization.data:200
msgid "Ports"
msgstr ""

#: ../../english/intro/organization.data:230
msgid "Special Configurations"
msgstr ""

#: ../../english/intro/organization.data:232
msgid "Laptops"
msgstr ""

#: ../../english/intro/organization.data:233
msgid "Firewalls"
msgstr ""

#: ../../english/intro/organization.data:234
msgid "Embedded systems"
msgstr ""

#: ../../english/intro/organization.data:249
msgid "Press Contact"
msgstr ""

#: ../../english/intro/organization.data:251
msgid "Web Pages"
msgstr ""

#: ../../english/intro/organization.data:263
msgid "Planet Debian"
msgstr ""

#: ../../english/intro/organization.data:268
msgid "Outreach"
msgstr ""

#: ../../english/intro/organization.data:273
msgid "Debian Women Project"
msgstr ""

#: ../../english/intro/organization.data:281
msgid "Community"
msgstr ""

#: ../../english/intro/organization.data:290
msgid ""
"To send a private message to all the members of the Community Team, use the "
"GPG key <a href=\"community-team-pubkey.txt"
"\">817DAE61E2FE4CA28E1B7762A89C4D0527C4C869</a>."
msgstr ""

#: ../../english/intro/organization.data:292
msgid "Events"
msgstr ""

#: ../../english/intro/organization.data:299
msgid "DebConf Committee"
msgstr ""

#: ../../english/intro/organization.data:306
msgid "Partner Program"
msgstr ""

#: ../../english/intro/organization.data:310
msgid "Hardware Donations Coordination"
msgstr ""

#: ../../english/intro/organization.data:325
msgid "GNOME Foundation"
msgstr ""

#: ../../english/intro/organization.data:327
msgid "Linux Professional Institute"
msgstr ""

#: ../../english/intro/organization.data:329
msgid "Linux Magazine"
msgstr ""

#: ../../english/intro/organization.data:331
msgid "Linux Standards Base"
msgstr ""

#: ../../english/intro/organization.data:333
msgid "Free Standards Group"
msgstr ""

#: ../../english/intro/organization.data:334
msgid "SchoolForge"
msgstr ""

#: ../../english/intro/organization.data:337
msgid ""
"OASIS: Organization\n"
"      for the Advancement of Structured Information Standards"
msgstr ""

#: ../../english/intro/organization.data:340
msgid ""
"OVAL: Open Vulnerability\n"
"      Assessment Language"
msgstr ""

#: ../../english/intro/organization.data:343
msgid "Open Source Initiative"
msgstr ""

#: ../../english/intro/organization.data:350
msgid "User support"
msgstr ""

#: ../../english/intro/organization.data:417
msgid "Bug Tracking System"
msgstr ""

#: ../../english/intro/organization.data:422
msgid "Mailing Lists Administration and Mailing List Archives"
msgstr ""

#: ../../english/intro/organization.data:431
msgid "New Members Front Desk"
msgstr ""

#: ../../english/intro/organization.data:437
msgid "Debian Account Managers"
msgstr ""

#: ../../english/intro/organization.data:441
msgid ""
"To send a private message to all DAMs, use the GPG key "
"57731224A9762EA155AB2A530CA8D15BB24D96F2."
msgstr ""

#: ../../english/intro/organization.data:442
msgid "Keyring Maintainers (PGP and GPG)"
msgstr ""

#: ../../english/intro/organization.data:446
msgid "Security Team"
msgstr ""

#: ../../english/intro/organization.data:457
msgid "Consultants Page"
msgstr ""

#: ../../english/intro/organization.data:462
msgid "CD Vendors Page"
msgstr ""

#: ../../english/intro/organization.data:465
msgid "Policy"
msgstr ""

#: ../../english/intro/organization.data:468
msgid "System Administration"
msgstr ""

#: ../../english/intro/organization.data:469
msgid ""
"This is the address to use when encountering problems on one of Debian's "
"machines, including password problems or you need a package installed."
msgstr ""

#: ../../english/intro/organization.data:479
msgid ""
"If you have hardware problems with Debian machines, please see <a href="
"\"https://db.debian.org/machines.cgi\">Debian Machines</a> page, it should "
"contain per-machine administrator information."
msgstr ""

#: ../../english/intro/organization.data:480
msgid "LDAP Developer Directory Administrator"
msgstr ""

#: ../../english/intro/organization.data:481
msgid "Mirrors"
msgstr ""

#: ../../english/intro/organization.data:488
msgid "DNS Maintainer"
msgstr ""

#: ../../english/intro/organization.data:489
msgid "Package Tracking System"
msgstr ""

#: ../../english/intro/organization.data:491
msgid "Treasurer"
msgstr ""

#: ../../english/intro/organization.data:498
msgid ""
"<a name=\"trademark\" href=\"m4_HOME/trademark\">Trademark</a> use requests"
msgstr ""

#: ../../english/intro/organization.data:502
msgid "Salsa administrators"
msgstr ""
