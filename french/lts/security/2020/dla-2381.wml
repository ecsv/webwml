#use wml::debian::translation-check translation="807e32d1da1926ddf87740a5eb99c45571c89324" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Une vulnérabilité a été découverte dans lua5.3, un langage de programmation
simple, extensible et intégrable, à cause de laquelle un dépassement devenant
négatif et une erreur de segmentation pourraient être provoqués dans getlocal et
setlocal, comme le montre getlocal(3,2^31).</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 5.3.3-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets lua5.3.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de lua5.3, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/lua5.3">https://security-tracker.debian.org/tracker/lua5.3</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2381.data"
# $Id: $
