#use wml::debian::translation-check translation="0472ab50b306d7e586556c9e40970467d110babf" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-16993">CVE-2019-16993</a>

<p>Dans phpBB, includes/acp/acp_bbcodes.php vérifiait de manière incorrecte
le jeton CSRF dans la page des BBCode dans Administration Control Panel. Une attaque
CSRF réelle était possible si un attaquant réussissait à récupérer l’identifiant
de session d’un administrateur réauthentifié avant de les cibler.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13776">CVE-2019-13776</a>

<p>phpBB permettait le vol de l’identifiant de session du Administration Control
Panel en exploitant un CSRF dans la fonction Remote Avatar. Le vol du jeton CSRF
aboutit à un XSS stocké.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 3.0.12-5+deb8u4.</p>
<p>Nous vous recommandons de mettre à jour vos paquets phpbb3.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1942.data"
# $Id: $
