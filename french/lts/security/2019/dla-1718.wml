#use wml::debian::translation-check translation="aa1da0d3ae63eb610830e024db0b1898e8deda1c" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Deux vulnérabilités ont été découvertes dans SQLALchemy, une boîte à outils
SQL et un mappeur objet-relationnel de Python.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-7164">CVE-2019-7164</a>

<p>SQLAlchemy permettait une injection SGL à l’aide du paramètre order_by.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-7548">CVE-2019-7548</a>

<p>SQLAlchemy possédait une injection SQL quand le paramètre group_by pouvait
être contrôlé.</p></li>

</ul>

<p>Le projet SQLAlchemy prévient que les correctifs de sécurité peuvent casser
la fonction de contrainte de texte rarement utilisée.</p>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 0.9.8+dfsg-0.1+deb8u1.
<p>Nous vous recommandons de mettre à jour vos paquets sqlalchemy.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1718.data"
# $Id: $
