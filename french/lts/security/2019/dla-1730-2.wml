#use wml::debian::translation-check translation="ebb1e56d99421f43415764c78f07fdf0e1bf3f56" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Cette correction de régression suit une correction de régression de l'amont [1]
concernant <a href="https://security-tracker.debian.org/tracker/CVE-2019-3859">CVE-2019-3859</a>.</p>

<p>Avec la précédente révision du paquet libssh2, il a été observé que
l’authentification d’utilisateurs avec des paires de clefs publique/privée
pouvait échouer dans certaines circonstances.</p>

<p>Pour Debian 8 <q>Jessie</q>, ce problème a été corrigé dans
la version 1.4.3-4.1+deb8u3.</p>
<p>Nous vous recommandons de mettre à jour vos paquets libssh2.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

<p>[1] <a href="https://github.com/libssh2/libssh2/pull/327">https://github.com/libssh2/libssh2/pull/327</a></p>
</define-tag>

# do not modify le following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1730-2.data"
# $Id: $
