#use wml::debian::translation-check translation="db28b03ffeea30dd379cb4120e6f83d0e85722d8" maintainer="galaxico"
#use wml::debian::mainpage title="Το διεθνές λειτουργικό σύστημα" GEN_TIME="yes"
#use wml::debian::recent_list
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/images.data"


<span class="download"><a href="<stable-images-url/>/amd64/iso-cd/debian-<current-tiny-cd-release-filename/>-amd64-netinst.iso">Μεταφορτώστε το Debian <current_release_short><em>(64-bit PC Network installer)</em></a> </span>

<div id="splash">
        <h1>Debian</h1>
</div>

<div id="intro">

<p>Το Debian είναι ένα
<a href="intro/free">ελεύθερο</a> λειτουργικό σύστημα (ΛΣ) για τον υπολογιστή
σας. Το λειτουργικό σύστημα είναι μια συλλογή από τα απαραίτητα προγράμματα
και βοηθήματα που απαιτεί ο υπολογιστής σας για να λειτουργήσει.
</p>

<p>Το Debian όμως προσφέρει κάτι παραπάνω από ένα καθαρό ΛΣ: 
περιέχει
περισσότερα από <packages_in_stable> <a HREF="distrib/packages">πακέτα</a>
προγραμμάτων, μεταγλωττισμένα και έτοιμα για εγκατάσταση και χρήση στο 
μηχάνημά σας.

<a href="intro/about">Διαβάστε περισσότερα...</a></p>

</div>

<hometoc/>

<p class="infobar">
H <a href="releases/stable/">τελευταία έκδοση του Debian</a> είναι η
<current_release_short>. Η τελευταία ανανέωση για αυτή την έκδοση έγινε στις
<current_release_date>. Διαβάστε περισσότερες πληροφορίες σχετικά με τις
<a href="releases/">διαθέσιμες εκδόσεις του Debian</a>.</p>


<h2>Ξεκινώντας με το Debian</h2>
<p>Παρακαλούμε χρησιμοποιείστε τη γραμμή πλοήγησης στην κορυφή αυτής της 
σελίδας για πρόσβατη σε περισσότερο περιεχόμενο.</p>
<p>Επιπλέον, χρήστες που μιλούν άλλες γλώσσες από τα Αγγλικά μπορούν να 
ελέγξουν την ενότητα <a href="international/">Διεθνή</a>, και άτομα με 
συστήματα άλλα από Intel x86 θα πρέπει να ελέγξουν την ενότητα <a 
href="ports/">Υλοποιήσεις (ports)</a>.</p>

<hr/>
<a class="rss_logo" href="News/news">RSS</a>
<h2>Νέα</h2>

<p><:= get_recent_list('News/$(CUR_YEAR)', '6', '$(ENGLISHDIR)', '', '\d+\w*' ) :></p>
<p>Για παλαιότερες ειδήσεις δείτε την σελίδα των <a href="$(HOME)/News/">Ειδήσεων</a>.
Αν θέλετε να λαμβάνετε με ηλ. ταχυδρομείο τις νεότερες εξελίξεις για το Debian, εγγραφείτε στην
<a href="MailingLists/debian-announce">debian-announce mailing list</a>.</p>

<hr/>
<a class="rss_logo" href="security/dsa">RSS</a>
<h2>Συμβουλές Ασφάλειας</h2>

<p><:= get_recent_list ('security/2w', '10', '$(ENGLISHDIR)', 'bydate', '(2000\d+\w+|dsa-\d+)' ) :></p>

<p>Για παλαιότερες συμβουλές ασφαλείας δείτε την σελίδα <a 
href="$(HOME)/security/">Σελίδα Ασφάλειας</a>.
Αν επιθυμείτε να λαμβάνετε συμβουλές ασφαλείας μόλις ανακοινώνονται, εγγραφείτε 
στη λίστα αλληλογραφίας
<a 
href="https://lists.debian.org/debian-security-announce/">debian-security-
announce</a>.</p>

{#rss#:
<link rel="alternate" type="application/rss+xml"
 title="Debian News" href="News/news">
<link rel="alternate" type="application/rss+xml"
 title="Debian Project News" href="News/weekly/dwn">
<link rel="alternate" type="application/rss+xml"
 title="Debian Security Advisories (titles only)" href="security/dsa">
<link rel="alternate" type="application/rss+xml"
 title="Debian Security Advisories (summaries)" href="security/dsa-long">
:#rss#}
