# Emmanuel Galatoulas <galas@tee.gr>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2019-07-14 02:10+0200\n"
"Last-Translator: Emmanuel Galatoulas <galas@tee.gr>\n"
"Language-Team: Greek <debian-www@lists.debian.org>\n"
"Language: el\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 2.0\n"

#: ../../english/template/debian/language_names.wml:8
msgid "Arabic"
msgstr "Αραβικά"

#: ../../english/template/debian/language_names.wml:9
msgid "Armenian"
msgstr "Αρμένικα"

#: ../../english/template/debian/language_names.wml:10
msgid "Finnish"
msgstr "Φινλανδικά"

#: ../../english/template/debian/language_names.wml:11
msgid "Croatian"
msgstr "Κροατικά"

#: ../../english/template/debian/language_names.wml:12
msgid "Danish"
msgstr "Δανέζικα"

#: ../../english/template/debian/language_names.wml:13
msgid "Dutch"
msgstr "Ολλανδικά"

#: ../../english/template/debian/language_names.wml:14
msgid "English"
msgstr "Αγγλικά"

#: ../../english/template/debian/language_names.wml:15
msgid "French"
msgstr "Γαλλικά"

#: ../../english/template/debian/language_names.wml:16
msgid "Galician"
msgstr "Γαλικίας"

#: ../../english/template/debian/language_names.wml:17
msgid "German"
msgstr "Γερμανικά"

#: ../../english/template/debian/language_names.wml:18
msgid "Italian"
msgstr "Ιταλικά"

#: ../../english/template/debian/language_names.wml:19
msgid "Japanese"
msgstr "Ιαπωνικά"

#: ../../english/template/debian/language_names.wml:20
msgid "Korean"
msgstr "Κορεάτικα"

#: ../../english/template/debian/language_names.wml:21
msgid "Spanish"
msgstr "Ισπανικά"

#: ../../english/template/debian/language_names.wml:22
msgid "Portuguese"
msgstr "Πορτογαλικά"

#: ../../english/template/debian/language_names.wml:23
msgid "Portuguese (Brazilian)"
msgstr "Πορτογαλικά (Βραζιλίας)"

#: ../../english/template/debian/language_names.wml:24
msgid "Chinese"
msgstr "Κινέζικα"

#: ../../english/template/debian/language_names.wml:25
msgid "Chinese (China)"
msgstr "Κινέζικα (Κίνας)"

#: ../../english/template/debian/language_names.wml:26
msgid "Chinese (Hong Kong)"
msgstr "Κινέζικα (Χονγκ Κονγκ)"

#: ../../english/template/debian/language_names.wml:27
msgid "Chinese (Taiwan)"
msgstr "Κινέζικα (Ταϊβάν)"

#: ../../english/template/debian/language_names.wml:28
msgid "Chinese (Traditional)"
msgstr "Κινέζικα (Παραδοσιακά)"

#: ../../english/template/debian/language_names.wml:29
msgid "Chinese (Simplified)"
msgstr "Κινέζικα (Απλοποιημένα)"

#: ../../english/template/debian/language_names.wml:30
msgid "Swedish"
msgstr "Σουηδικά"

#: ../../english/template/debian/language_names.wml:31
msgid "Polish"
msgstr "Πολωνικά"

#: ../../english/template/debian/language_names.wml:32
msgid "Norwegian"
msgstr "Νορβηγικά"

#: ../../english/template/debian/language_names.wml:33
msgid "Turkish"
msgstr "Τουρκικά"

#: ../../english/template/debian/language_names.wml:34
msgid "Russian"
msgstr "Ρώσικα"

#: ../../english/template/debian/language_names.wml:35
msgid "Czech"
msgstr "Τσέχικα"

#: ../../english/template/debian/language_names.wml:36
msgid "Esperanto"
msgstr "Εσπεράντο"

#: ../../english/template/debian/language_names.wml:37
msgid "Hungarian"
msgstr "Ουγγαρέζικα"

#: ../../english/template/debian/language_names.wml:38
msgid "Romanian"
msgstr "Ρουμάνικα"

#: ../../english/template/debian/language_names.wml:39
msgid "Slovak"
msgstr "Σλοβάκικα"

#: ../../english/template/debian/language_names.wml:40
msgid "Greek"
msgstr "Ελληνικά"

#: ../../english/template/debian/language_names.wml:41
msgid "Catalan"
msgstr "Καταλωνικά"

#: ../../english/template/debian/language_names.wml:42
msgid "Indonesian"
msgstr "Ινδονησιακά"

#: ../../english/template/debian/language_names.wml:43
msgid "Lithuanian"
msgstr "Λιθουανικά"

#: ../../english/template/debian/language_names.wml:44
msgid "Slovene"
msgstr "Σλοβάκικα"

#: ../../english/template/debian/language_names.wml:45
msgid "Bulgarian"
msgstr "Βουλγάρικα"

#: ../../english/template/debian/language_names.wml:46
msgid "Tamil"
msgstr "Ινδικά Tamil"

#. for now, the following are only needed if you intend to translate intl/l10n
#: ../../english/template/debian/language_names.wml:48
msgid "Afrikaans"
msgstr "Αφρικανικά"

#: ../../english/template/debian/language_names.wml:49
msgid "Albanian"
msgstr "Αζερμπαϊτζανικά"

#: ../../english/template/debian/language_names.wml:50
msgid "Asturian"
msgstr "Αστουρίας"

#: ../../english/template/debian/language_names.wml:51
msgid "Amharic"
msgstr "Αραμαϊκά"

#: ../../english/template/debian/language_names.wml:52
msgid "Azerbaijani"
msgstr "Αζερμπαϊτζανικά"

#: ../../english/template/debian/language_names.wml:53
msgid "Basque"
msgstr "Βάσκικα"

#: ../../english/template/debian/language_names.wml:54
msgid "Belarusian"
msgstr "Λευκορωσικά"

#: ../../english/template/debian/language_names.wml:55
msgid "Bengali"
msgstr "Μπενγκάλι"

#: ../../english/template/debian/language_names.wml:56
msgid "Bosnian"
msgstr "Βοσνιακά"

#: ../../english/template/debian/language_names.wml:57
msgid "Breton"
msgstr "Βρετονικά"

#: ../../english/template/debian/language_names.wml:58
msgid "Cornish"
msgstr "Κορνουάλης"

#: ../../english/template/debian/language_names.wml:59
msgid "Estonian"
msgstr "Εσθονικά"

#: ../../english/template/debian/language_names.wml:60
msgid "Faeroese"
msgstr "Νήσων Φαρόε"

#: ../../english/template/debian/language_names.wml:61
msgid "Gaelic (Scots)"
msgstr "Κελτικά Σκωτίας"

#: ../../english/template/debian/language_names.wml:62
msgid "Georgian"
msgstr "Γεωργιανά"

#: ../../english/template/debian/language_names.wml:63
msgid "Hebrew"
msgstr "Εβραϊκά"

#: ../../english/template/debian/language_names.wml:64
msgid "Hindi"
msgstr "Κεντρικής Ινδίας"

#: ../../english/template/debian/language_names.wml:65
msgid "Icelandic"
msgstr "Ισλανδικά"

#: ../../english/template/debian/language_names.wml:66
msgid "Interlingua"
msgstr "Interlingua"

#: ../../english/template/debian/language_names.wml:67
msgid "Irish"
msgstr "Ιρλανδικά"

#: ../../english/template/debian/language_names.wml:68
msgid "Kalaallisut"
msgstr "Kalaallisut"

#: ../../english/template/debian/language_names.wml:69
msgid "Kannada"
msgstr "Kannada"

#: ../../english/template/debian/language_names.wml:70
msgid "Kurdish"
msgstr "Κουρδικά"

#: ../../english/template/debian/language_names.wml:71
msgid "Latvian"
msgstr "Λεττονικά"

#: ../../english/template/debian/language_names.wml:72
msgid "Macedonian"
msgstr "Σλαβομακεδονικά"

#: ../../english/template/debian/language_names.wml:73
msgid "Malay"
msgstr "Μαλαισίας"

#: ../../english/template/debian/language_names.wml:74
msgid "Malayalam"
msgstr "Malayalam"

#: ../../english/template/debian/language_names.wml:75
msgid "Maltese"
msgstr "Μαλτέζικα"

#: ../../english/template/debian/language_names.wml:76
msgid "Manx"
msgstr "Νήσου Μαν"

#: ../../english/template/debian/language_names.wml:77
msgid "Maori"
msgstr "Μαορί"

#: ../../english/template/debian/language_names.wml:78
msgid "Mongolian"
msgstr "Μογγολικά"

#: ../../english/template/debian/language_names.wml:80
msgid "Norwegian Bokm&aring;l"
msgstr "Νορβηγικά Bokm&aring;l"

#: ../../english/template/debian/language_names.wml:82
msgid "Norwegian Nynorsk"
msgstr "Νορβηγικά Nynorsk"

#: ../../english/template/debian/language_names.wml:83
msgid "Occitan (post 1500)"
msgstr "Οξιτανικά (μετά το 1500)"

#: ../../english/template/debian/language_names.wml:84
msgid "Persian"
msgstr "Περσικά"

#: ../../english/template/debian/language_names.wml:85
msgid "Serbian"
msgstr "Σερβικά"

#: ../../english/template/debian/language_names.wml:86
msgid "Slovenian"
msgstr "Σλοβάκικα"

#: ../../english/template/debian/language_names.wml:87
msgid "Tajik"
msgstr "Tajik"

#: ../../english/template/debian/language_names.wml:88
msgid "Thai"
msgstr "Ταϊλανδικά"

#: ../../english/template/debian/language_names.wml:89
msgid "Tonga"
msgstr "Tonga"

#: ../../english/template/debian/language_names.wml:90
msgid "Twi"
msgstr "Twi"

#: ../../english/template/debian/language_names.wml:91
msgid "Ukrainian"
msgstr "Ουκρανικά"

#: ../../english/template/debian/language_names.wml:92
msgid "Vietnamese"
msgstr "Βιετναμικά"

#: ../../english/template/debian/language_names.wml:93
msgid "Welsh"
msgstr "Ουαλλικά"

#: ../../english/template/debian/language_names.wml:94
msgid "Xhosa"
msgstr "Xhosa"

#: ../../english/template/debian/language_names.wml:95
msgid "Yiddish"
msgstr "Γίντις"

#: ../../english/template/debian/language_names.wml:96
msgid "Zulu"
msgstr "Ζουλού"

#~ msgid "Gallegan"
#~ msgstr "Ισπανική διάλεκτος Γαλικίας"
